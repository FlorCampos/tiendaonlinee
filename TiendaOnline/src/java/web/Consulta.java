package web;

import entidades.DB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ConsultaTvs", urlPatterns = {"/ConsultaTvs"})
public class Consulta extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         PrintWriter out = response.getWriter();
        response.setContentType("text/html;charset=UTF-8");
        out.println("<!DOCTYPE html>\n" +
"<html>\n" +
"<head>\n" +
"    <meta charset=\"utf-8\">\n" +
"  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n" +
"  <meta name=\"description\" content=\"\">\n" +
"  <meta name=\"author\" content=\"\">\n" +
"  <title>Tienda Online</title>\n" +
"  	<link rel=\"stylesheet\" type=\"text/css\" href=\"fonts/font-awesome-4.7.0/css/font-awesome.min.css\">\n" +
"	<link rel=\"stylesheet\" type=\"text/css\" href=\"fonts/iconic/css/material-design-iconic-font.min.css\">\n" +
"	<link rel=\"stylesheet\" type=\"text/css\" href=\"vendor/animate/animate.css\">\n" +
"	<link rel=\"stylesheet\" type=\"text/css\" href=\"vendor/css-hamburgers/hamburgers.min.css\">\n" +
"	<link rel=\"stylesheet\" type=\"text/css\" href=\"vendor/animsition/css/animsition.min.css\">\n" +
"	<link rel=\"stylesheet\" type=\"text/css\" href=\"vendor/select2/select2.min.css\">\n" +
"	<link rel=\"stylesheet\" type=\"text/css\" href=\"vendor/daterangepicker/daterangepicker.css\">\n" +
"        <link href=\"vendor/bootstrap/css/bootstrap.css\" rel=\"stylesheet\">\n" +
"        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css\"/>\n" +
"        <link href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css\" rel=\"stylesheet\"  crossorigin=\"anonymous\">\n" +
"        <link href=\"css/productos.css\" rel=\"stylesheet\">\n" +
"</head>\n" +
"<body>\n" +
"    <nav class=\"navbar navbar-light bg-light header\">\n" +     
"    <a class=\"navbar-brand\">Compras Web</a>\n" + 
"<div class=\"wrap-input100 validate-input\" data-validate = \"Enter username\">\n" +
"   <input class=\"input100\" type=\"text\" name=\"nombre\" placeholder=\"Nombre\">\n" +
"   <button class=\"login100-form-btn\" id=\"crear\" type=\"submit\" value=\"Crear\">Search</button>\n" +        
"</div>\n" +                
"    </nav>\n" +

"    <header></header>\n" +                
"    <main>\n" +           
"    <div class=\"container\">" +           
"    <div class=\"row\">");


  
        String consultaSql = "SELECT * FROM productos ORDER BY id DESC LIMIT 6";
        Connection miConexion = null;
        try {
            miConexion = DB.getInstance().getConnection();
            PreparedStatement miPreparativo = miConexion.prepareStatement(consultaSql);
            ResultSet miResultado =  miPreparativo.executeQuery();
            while (miResultado.next()) {
                Producto miProducto = new Producto();
                miProducto.nombre = miResultado.getString("nombre");
                miProducto.precio = miResultado.getString("precio");
                miProducto.cantidad = miResultado.getString("cantidad");
                miProducto.descripcion = miResultado.getString("descripcion");
                miProducto.foto = miResultado.getString("foto");
                System.out.println(miProducto);

                out.println("<div class=\"col-lg-4 col-sm-6 mb-4\">");
                out.println("<div class=\"card bg-dark text-white\">");
                out.println("<img src=' " + miProducto.foto + "' class=\"card-img\" />");
                out.println("<div class=\"card-img-price\">");                
                out.println("<h5 class=\"card-price\">$" + miProducto.precio + " </h5>");
                out.println("</div>");
                out.println("<div class=\"card-img-overlay animate__fadeInUp\">");                
                out.println("<h5 class=\"card-title\"> " + miProducto.nombre + "</h5>");
                out.println("<p> " + miProducto.descripcion + " </p>");
            /*  out.println("<div>Cantidad: " + miProducto.cantidad + " </div>"); */
                out.println("</div>"); 
                out.println("</div>"); 
                out.println("</div>"); 

            }
            
        } catch (ClassNotFoundException miExepcion) {
            System.out.println(miExepcion);
        } catch (SQLException miExepcion) {
            System.out.println(miExepcion);
        } finally {
            try {
                if (miConexion != null) {
                    miConexion.close();
                }

            } catch (SQLException miExepcion) {
                System.out.println(miExepcion);
            }
        }
        out.println("</main>\n" +
"    <footer></footer>\n" +
"   <div id=\"dropDownSelect1\"></div>\n" +
"  	<script src=\"vendor/jquery/jquery-3.2.1.min.js\"></script>\n" +
"	<script src=\"vendor/animsition/js/animsition.min.js\"></script>\n" +
"	<script src=\"vendor/bootstrap/js/popper.js\"></script>\n" +
"	<script src=\"vendor/bootstrap/js/bootstrap.min.js\"></script>\n" +
"	<script src=\"vendor/select2/select2.min.js\"></script>\n" +
"	<script src=\"vendor/daterangepicker/moment.min.js\"></script>\n" +
"	<script src=\"vendor/daterangepicker/daterangepicker.js\"></script>\n" +
"	<script src=\"vendor/countdowntime/countdowntime.js\"></script>\n" +
"	<script src=\"js/main.js\"></script>\n" +
"	<script src=\"particles.js\"></script>\n" +
"	<script src=\"js/app.js\"></script>\n" +
"	<script src=\"js/lib/stats.js\"></script>\n" +                
"</body>\n" +
"</html>");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
