-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-07-2020 a las 23:48:42
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tiendaonline`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `Nombre` varchar(255) NOT NULL,
  `Descripcion` varchar(255) NOT NULL,
  `Precio` int(255) NOT NULL,
  `Cantidad` int(255) NOT NULL,
  `Foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `Nombre`, `Descripcion`, `Precio`, `Cantidad`, `Foto`) VALUES
(1, 'Cuadro Tríptico Evangelion ', 'Tamaño : 40x50cm ', 2400, 8, 'https://d26lpennugtm8s.cloudfront.net/stores/336/406/products/mock-up-triptico-evangelion-131-34e527ec5bfe951bbd15362609386270-1024-1024.jpg'),
(7, 'Cuadro Tríptico Tokyo Ghoul', 'Tamaño : 40x50cm ', 2400, 5, 'https://d26lpennugtm8s.cloudfront.net/stores/336/406/products/tokyo-ghoul1-57709076453ffa9a6a14896238433344-480-0.jpg'),
(8, 'Cuadro Tríptico Tokyo Ghoul 2', 'Tamaño : 40x50cm ', 2400, 5, 'https://d26lpennugtm8s.cloudfront.net/stores/336/406/products/foto-10-7-18-17-06-451-0cda99f7ca7ccdfe4315313993457290-1024-1024.jpg'),
(9, 'Cuadro Tríptico La Gran Ola de Kanagawa - Hokusai', 'Tamaño : 30x44cm ', 2300, 5, 'https://d26lpennugtm8s.cloudfront.net/stores/336/406/products/mockup-la-ola-triptico1-559c369e0af9886fe614997098476967-1024-1024.jpg'),
(10, 'Cuadro Evangelion - Lanza de Longinus', 'Tamaño : 20x28cm ', 2400, 9, 'https://d26lpennugtm8s.cloudfront.net/stores/336/406/products/mock-up-cuadro-con-marco-horizontal-lanza-de-longinus-40x551-53e6de124926ae54d515362484447577-1024-1024.jpg'),
(11, 'Cuadro Evangelion ', 'Tamaño : 20x28cm ', 1100, 9, 'https://d26lpennugtm8s.cloudfront.net/stores/336/406/products/mock-up-cuadro-con-marco-horizontal-evangelion-12-55x801-e209897788337f7b1915362614941213-1024-1024.jpg'),
(20, 'Cuadro Tríptico Cowboy Bebop', 'Tamaño : 20x28cm ', 1100, 9, 'https://d26lpennugtm8s.cloudfront.net/stores/336/406/products/foto-10-7-18-17-06-451-0cda99f7ca7ccdfe4315313993457290-480-0.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
